#version 330
uniform mat4 mvp;
out vec2 frag_uv_ss;
in vec2 quad_cs;
void main() {
	frag_uv_ss = quad_cs*0.5 + 0.5;
	gl_Position = mvp*vec4(vec3(quad_cs, 0.0), 1.0);
}
