#version 330
uniform mat4 mvp;
in vec2 quad_cs;
void main() {
	gl_Position = mvp*vec4(vec3(quad_cs, 0.0), 1.0);
}
