#version 330
uniform sampler2D tex;
uniform vec2 uv_scale;
uniform vec2 uv_offset;
in vec2 frag_uv_ss;
out vec4 out_color;
void main() {
	out_color = texture(tex, uv_scale * frag_uv_ss + uv_offset);
}
