#ifndef GRAPHICS_H
#define GRAPHICS_H

/* make sure to include gfx/context.h before glfw */
#include "gfx/context.h"
#include "gfx/assert.h"
#include "gfx/shader.h"
#include "gfx/texture.h"
#include "gfx/vertex_array.h"
#include "gfx/vertex_buffer.h"
#include "gfx/mesh.h"
#include "gfx/frame_buffer.h"

#include <GLFW/glfw3.h>

#include "utils.h"

#include "dat/tex/c64.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern GLFWwindow* window;

extern mesh2d_t quad;
extern mesh2d_t triangle;
extern mesh2d_t debris;
extern mesh2d_t subdebris[];
extern texture2d_t c64_texture;

extern program_t prog_poly;
extern uniform_t prog_poly_mvp;
extern attribute_t prog_poly_quad_cs;

extern program_t prog_blit;
extern uniform_t prog_blit_mvp;
extern uniform_t prog_blit_tex;
extern uniform_t prog_blit_uv_scale;
extern uniform_t prog_blit_uv_offset;
extern attribute_t prog_blit_quad_cs;

/* always pair setup_graphics with setdown_graphics (if successful) */
int setup_graphics();
/* can be called at any time in between setup and setdown */
int load_programs();
void create_programs();
void destroy_programs();
void setdown_graphics();

#endif
