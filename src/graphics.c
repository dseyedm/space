#include "graphics.h"

/* quad
 _
|_|
*/
vec2 quad_data[] = {
	{-1.0,-1.0 },
	{ 1.0,-1.0 },
	{-1.0, 1.0 },
	{ 1.0, 1.0 }
};

/* ship
  2
  /\
0/__\1
*/
vec2 triangle_data[] = {
	{-1.0,-1.0 },
	{ 1.0,-1.0 },
	{ 0.0, 1.0 }
};

/* debris, render with GL_LINE_LOOP
  3
  /\
0/  \2
 \  /
  \/
  1
*/
vec2 debris_data[] = {
	{-1.0, 0.0 },
	{ 0.0,-1.0 },
	{ 1.0, 0.0 },
	{ 0.0, 1.0 }
};

/* generated cracks, centered
__
\ |
 \|
 __
| /
|/

|\
|_\

 /|
/_|
*/
vec2 subdebris_data[SIZE(debris_data) * 3] = {
	/* bottom left */
	{-0.5, 0.5 },
	{ 0.5,-0.5 },
	{ 0.5, 0.5 },
	/* bottom right */
	{-0.5, 0.5 },
	{-0.5,-0.5 },
	{ 0.5, 0.5 },
	/* top right */
	{-0.5, 0.5 },
	{-0.5,-0.5 },
	{ 0.5,-0.5 },
	/* top left */
	{-0.5,-0.5 },
	{ 0.5,-0.5 },
	{ 0.5, 0.5 }
};

int load_program(program_t* program, const char* vert_src, const char* frag_src) {
	gfx_assert(program->handle != GLHANDLE);

	shader_t vert = ISHADER, frag = ISHADER;
	create_shader(&vert, GL_VERTEX_SHADER);
	create_shader(&frag, GL_FRAGMENT_SHADER);

	/* compile */
	const char* vert_result = compile_shader(vert, vert_src);
	if(vert_result != NULL) {
		fprintf(stderr, "failed to load vertex shader:\n%s", vert_result);
		free((char*)vert_result);
		destroy_shader(&vert);
		destroy_shader(&frag);
		return 0;
	}

	const char* frag_result = compile_shader(frag, frag_src);
	if(frag_result != NULL) {
		fprintf(stderr, "failed to load fragment shader:\n%s", frag_result);
		free((char*)frag_result);
		destroy_shader(&vert);
		destroy_shader(&frag);
		return 0;
	}

	/* link */
	const char* link_result = link_program(*program, vert, frag);
	if(link_result != NULL) {
		fprintf(stderr, "failed to link program:\n%s", link_result);
		free((char*)link_result);
		destroy_shader(&vert);
		destroy_shader(&frag);
		return 0;
	}

	destroy_shader(&vert);
	destroy_shader(&frag);

	return 1;
}

int load_program_files(program_t* program, const char* vert_filename, const char* frag_filename) {
	char* vert_contents = load_file(vert_filename);
	if(!vert_contents) {
		return 0;
	}

	char* frag_contents = load_file(frag_filename);
	if(!frag_contents) {
		free(vert_contents);
		return 0;
	}

	int ret = load_program(program, vert_contents, frag_contents);

	free(vert_contents);
	free(frag_contents);

	if(!ret) fprintf(stderr, "failed to load %s or %s\n", vert_filename, frag_filename);

	return ret;
}

GLFWwindow* window = NULL;

/* render a front-facing quad using render_mesh2d_vertices(&quad, GL_TRIANGLE_STRIP); */
mesh2d_t quad = IMESH2D;
mesh2d_t triangle = IMESH2D;
mesh2d_t debris = IMESH2D;
mesh2d_t subdebris[] = {
	IMESH2D, IMESH2D, IMESH2D, IMESH2D
};
texture2d_t c64_texture = ITEXTURE2D;

program_t prog_poly = IPROGRAM;
uniform_t prog_poly_mvp = IUNIFORM;
attribute_t prog_poly_quad_cs = IATTRIBUTE;

program_t prog_blit = IPROGRAM;
uniform_t prog_blit_mvp = IUNIFORM;
uniform_t prog_blit_tex = IUNIFORM;
uniform_t prog_blit_uv_scale = IUNIFORM;
uniform_t prog_blit_uv_offset = IUNIFORM;
attribute_t prog_blit_quad_cs = IATTRIBUTE;

int load_programs() {
	/* poly shader */
	gfx_assert(load_program_files(&prog_poly, "dat/poly.vert", "dat/white.frag"));
	prog_poly_mvp = get_uniform(prog_poly, "mvp");
	prog_poly_quad_cs = get_attribute(prog_poly, "quad_cs");
	bind_program(prog_poly);
	attach_mesh2d(&quad, (attribute_t*) {
		&prog_poly_quad_cs
	});
	attach_mesh2d(&triangle, (attribute_t*) {
		&prog_poly_quad_cs
	});
	attach_mesh2d(&debris, (attribute_t*) {
		&prog_poly_quad_cs
	});
	for(int i = 0; i < SIZE(subdebris); ++i) {
		attach_mesh2d(&subdebris[i], (attribute_t*) {
			&prog_poly_quad_cs
		});
	}
	unbind_program();

	/* blit shader */
	gfx_assert(load_program_files(&prog_blit, "dat/blit.vert", "dat/blit.frag"));
	prog_blit_mvp = get_uniform(prog_blit, "mvp");
	prog_blit_tex = get_uniform(prog_blit, "tex");
	prog_blit_uv_offset = get_uniform(prog_blit, "uv_offset");
	prog_blit_uv_scale = get_uniform(prog_blit, "uv_scale");
	prog_blit_quad_cs = get_attribute(prog_blit, "quad_cs");
	bind_program(prog_blit);
	attach_mesh2d(&quad, (attribute_t*) {
		&prog_blit_quad_cs
	});
	unbind_program();

	return 1;
}

int setup_graphics() {
	/* init glfw */
	if(!glfwInit()) {
		fprintf(stderr, "failed to init glfw.\n");
		return 0;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	/* create the glfw window before gl calls */
	glfwWindowHint(GLFW_RESIZABLE, 0);
	window = glfwCreateWindow(WIDTH, HEIGHT, "demo", NULL, NULL);
	if(window == NULL) {
		fprintf(stderr, "failed to open glfw window. is opengl 3.3 supported?\n");
		glfwTerminate();
		return 0;
	}
	glfwPollEvents();
	glfwMakeContextCurrent(window);

	/* initialize the opengl context */
	if(gl3wInit() != 0) {
		fprintf(stderr, "failed to initialize gl3w. is opengl 3.3 supported?\n");
		glfwDestroyWindow(window);
		glfwTerminate();
		return 0;
	}

	/* make sure 3.3 is supported */
	if(!gl3wIsSupported(3, 3)) {
		fprintf(stderr, "opengl 3.3 is not supported.\n");
		glfwDestroyWindow(window);
		glfwTerminate();
		return 0;
	}

	/* quad */
	create_mesh2d(&quad);
	quad.vertices_size = SIZE(quad_data);
	quad.vertices = quad_data;
	upload_mesh2d(&quad);

	/* triangle */
	create_mesh2d(&triangle);
	triangle.vertices_size = SIZE(triangle_data);
	triangle.vertices = triangle_data;
	upload_mesh2d(&triangle);

	/* debris */
	create_mesh2d(&debris);
	debris.vertices_size = SIZE(debris_data);
	debris.vertices = debris_data;
	upload_mesh2d(&debris);

	for(int i = 0; i < SIZE(subdebris); ++i) {
		create_mesh2d(&subdebris[i]);
		subdebris[i].vertices_size = 3;
		subdebris[i].vertices = &subdebris_data[i*3];
		upload_mesh2d(&subdebris[i]);
	}

	/* texture */
	create_texture2d(&c64_texture);
	bind_texture2d(c64_texture);
	set_filter_texture2d(GL_NEAREST);
	set_wrap_texture2d(GL_CLAMP_TO_BORDER);
	/* by default opengl expects the # of bytes in each row to be a multiple of 4 */
	/* let opengl know that this texture doesn't have such padding */
	gl(PixelStorei(GL_UNPACK_ALIGNMENT, 1));
	/* the 8 bit font data describes the alpha channel, tell opengl that */
	GLint font_mask[] = { GL_ONE, GL_ONE, GL_ONE, GL_RED };
	gl(TexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, font_mask));
	upload_texture2d(&c64_font[0], C64_X * strlen(c64_font_content), C64_Y, GL_UNSIGNED_BYTE, GL_RED, GL_R8);
	gl(PixelStorei(GL_UNPACK_ALIGNMENT, 4));
	unbind_texture2d();

	/* create programs */
	create_program(&prog_poly);
	create_program(&prog_blit);

	return load_programs();
}

void setdown_graphics() {
	/* destroy opengl objects */
	destroy_mesh2d(&quad);
	destroy_mesh2d(&triangle);
	destroy_mesh2d(&debris);
	for(int i = 0; i < SIZE(subdebris); ++i) {
		destroy_mesh2d(&subdebris[i]);
	}
	destroy_texture2d(&c64_texture);

	/* destroy programs */
	destroy_program(&prog_poly);
	destroy_program(&prog_blit);

	/* destroy window after gl calls */
	glfwDestroyWindow(window);
	window = NULL;

	glfwTerminate();
}
