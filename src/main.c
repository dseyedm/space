/*
 * note "poly" refers to a collision shape represented as a list of world space points
 * polys are rendered using GL_LINE_LOOP
 * polys are convex
 */

#include "graphics.h"

#include <time.h>

typedef struct {
	/* where (0.0, 0.0) is bottom left */
	/* rads from top of clock leftwards */
	double x, y, scale_x, scale_y, rads;
} transform_t;
#define IFORM { 0.0, 0.0, 1.0, 1.0, 0.0 }

typedef struct {
	transform_t form;
	double velocity_x, velocity_y;
	/* in rads */
	double rotate_velocity;
} object_t;
#define IOBJECT { { WIDTH / 2.0, HEIGHT / 2.0, 16.0, 16.0*2.0, 0.0 }, 0.0, 0.0, 0.0 }

typedef struct {
	object_t o;
	int i; /* corresponds to mesh_subdebris[] */
} subdebris_t;

/* out_axes must be n length */
void calc_poly_axes(vec2* poly, int n, vec2* out_axes) {
	for(int i = 0; i < n; ++i) {
		vec2 edge;
		vec2_sub(edge, poly[i], poly[(i + 1)%n]);
		/* store the perpendicular */
		out_axes[i][0] =-edge[1];
		out_axes[i][1] = edge[0];
		vec2_norm(out_axes[i], out_axes[i]);
	}
}

/* note, axis must be normalized! */
void proj_poly_on_axis(vec2* poly, int n, vec2 axis, vec2 out_proj) {
	double minimum = vec2_dot(axis, poly[0]);
	double maximum = minimum;
	for(int i = 1; i < n; ++i) {
		double p = vec2_dot(axis, poly[i]);
		if(p < minimum) {
			minimum = p;
		} else if(p > maximum) {
			maximum = p;
		}
	}
	out_proj[0] = minimum;
	out_proj[1] = maximum;
}

/* where x < y */
static inline int proj_overlap(vec2 proj0, vec2 proj1) {
	return proj0[0] < proj1[0] ? proj1[0] < proj0[1] :
		   proj0[0] < proj1[1];
}

/* do two polygons intersect? */
int poly_poly_intersect(vec2* poly0, int n0, vec2* poly1, int n1) {
	vec2 axes[n0 + n1];
	calc_poly_axes(poly0, n0, axes);
	calc_poly_axes(poly1, n1, axes + n0);
	for(int i = 0; i < n0 + n1; ++i) {
		vec2 proj0, proj1;
		proj_poly_on_axis(poly0, n0, axes[i], proj0);
		proj_poly_on_axis(poly1, n1, axes[i], proj1);
		if(!proj_overlap(proj0, proj1)) return 0;
	}
	return 1;
}

/* transform a polygon */
void transform_poly(vec2* poly, int n, transform_t* form, vec2* out_poly) {
	for(int i = 0; i < n; ++i) {
		/* reset */
		out_poly[i][0] = poly[i][0];
		out_poly[i][1] = poly[i][1];
		/* scale */
		out_poly[i][0] *= form->scale_x;
		out_poly[i][1] *= form->scale_y;
		/* rotate */
		vec2_rotate(out_poly[i], form->rads);
		/* translate */
		out_poly[i][0] += form->x;
		out_poly[i][1] += form->y;
	}
}

/* calculates the mid point of a poly using average */
void calc_poly_centroid(vec2* poly, int n, vec2 out_centroid) {
	vec2 centroid;
	centroid[0] = poly[0][0];
	centroid[1] = poly[0][1];
	for(int i = 1; i < n; ++i) {
		centroid[0] += poly[i][0];
		centroid[1] += poly[i][1];
	}
	out_centroid[0] = centroid[0]/n;
	out_centroid[1] = centroid[1]/n;
}

/* form debris triangles using the poly's centroid */
void crack_poly(vec2* poly, int n, vec2* out_n_tris) {
	vec2 centroid;
	calc_poly_centroid(poly, n, centroid);
	for(int i = 0; i < n; ++i) {
		/* i */
		out_n_tris[i*3][0] = poly[i][0];
		out_n_tris[i*3][1] = poly[i][1];
		/* i + 1 */
		out_n_tris[i*3 + 1][0] = poly[(i + 1)%n][0];
		out_n_tris[i*3 + 1][1] = poly[(i + 1)%n][1];
		/* centroid */
		out_n_tris[i*3 + 2][0] = centroid[0];
		out_n_tris[i*3 + 2][1] = centroid[1];
	}
}

/* calculates mvp */
void push_mvp(mat4x4 vp, transform_t* form, uniform_t uniform) {
	mat4x4 mat;
	mat4x4_identity(mat);
	mat4x4_rotate_Z(mat, mat, form->rads);
	mat4x4_scale_X(mat, form->scale_x);
	mat4x4_scale_Y(mat, form->scale_y);
	mat4x4_translate(mat, form->x, form->y, 0.0);
	mat4x4_mul(mat, vp, mat);
	set_uniform4x4f(uniform, mat);
}

/* x,y specifies top left of starting character */
void render_text(const char* str, mat4x4 vp, transform_t* in_form) {
	int strlen_c64_font_content = strlen(c64_font_content);
	double one_over_strlen_c64_font_content = 1.0 / strlen_c64_font_content;
	int strlen_str = strlen(str);

	transform_t form = *in_form;
	form.x += form.scale_x;
	form.y -= 2.0*form.scale_y;

	bind_program(prog_blit);
	set_uniform1i(prog_blit_tex, active_texture(0));
	bind_texture2d(c64_texture);

	set_uniform2f(
		prog_blit_uv_scale,
		(vec2) { one_over_strlen_c64_font_content, 1.0 }
	);

	for(int i = 0; i < strlen_str; ++i) {
		if(str[i] == ' ') {
			form.x += 2.0*form.scale_x;
			continue;
		} else if(str[i] == '\n') {
			form.x = in_form->x + form.scale_x;
			form.y -= 2.0*form.scale_y + form.scale_y;
			continue;
		}

		int j = 0;
		while(j < strlen_c64_font_content && c64_font_content[j] != str[i]) {
			++j;
		}
		gfx_assert(j < strlen_c64_font_content);

		push_mvp(vp, &form, prog_blit_mvp);
		set_uniform2f(
			prog_blit_uv_offset,
			(vec2) { j * one_over_strlen_c64_font_content, 0.0 }
		);

		form.x += 2.0*form.scale_x;

		render_mesh2d_vertices(&quad, GL_TRIANGLE_STRIP);
	}

	unbind_texture2d();
	unbind_program();
}

#define MAX_BULLETS 64
#define MAX_DEBRIS 16
#define MAX_SUBDEBRIS MAX_DEBRIS*4

#define SCORE_DEBRIS 11
#define SCORE_SUBDEBRIS 19

#if 0 /* arrows */
#define BUTTON_UP GLFW_KEY_UP
#define BUTTON_LEFT GLFW_KEY_LEFT
#define BUTTON_RIGHT GLFW_KEY_RIGHT
#define BUTTON_FIRE GLFW_KEY_SPACE
#else /* wasd */
#define BUTTON_UP GLFW_KEY_W
#define BUTTON_LEFT GLFW_KEY_A
#define BUTTON_RIGHT GLFW_KEY_D
#define BUTTON_FIRE GLFW_KEY_SPACE
#endif
#define BUTTON_RESTART GLFW_KEY_R
#define BUTTON_PAUSE GLFW_KEY_T
#define BUTTON_SLOW GLFW_KEY_LEFT_SHIFT

int main() {
	srand(0x1337);

	if(!setup_graphics()) return 1;

	/* set opengl settings */
	gl(Disable(GL_CULL_FACE));
	gl(CullFace(GL_BACK));
	gl(Enable(GL_BLEND));
	gl(BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
	gl(ClearColor(0.0, 0.0, 0.0, 1.0));

	int max_score = 0;

restart:;

	object_t ship = IOBJECT;
	ship.form.scale_x *= 0.4;
	ship.form.scale_y *= 0.4;
	vec2 ship_poly[triangle.vertices_size];
	transform_poly(triangle.vertices, triangle.vertices_size, &ship.form, ship_poly);

	object_t obullets[MAX_BULLETS];
	vec2 bullet_polys[SIZE(obullets)][triangle.vertices_size];
	int obullets_n = 0;

	object_t odebris[MAX_DEBRIS];
	vec2 debris_polys[SIZE(odebris)][debris.vertices_size];
	int odebris_n = MAX_DEBRIS;
	for(int i = 0; i < odebris_n; ++i) {
		odebris[i].form.x = rand()%WIDTH;
		odebris[i].form.y = rand()%HEIGHT;
		odebris[i].form.scale_x = (rand()%1500)/100.0 + 20;
		odebris[i].form.scale_y = (rand()%1500)/100.0 + 20;
		odebris[i].rotate_velocity = (rand()%2000)/2000.0*0.5*PI - 0.25*PI;
		odebris[i].velocity_x = (rand()%500)/5.0 - 50.0;
		odebris[i].velocity_y = (rand()%500)/5.0 - 50.0;
		transform_poly(debris.vertices, debris.vertices_size, &odebris[i].form, debris_polys[i]);
		vec2 centroid;
		calc_poly_centroid(debris_polys[i], debris.vertices_size, centroid);
		if(dist2(centroid, (vec2){ ship.form.x, ship.form.y }) <= 100.0*100.0
		/*|| poly_poly_intersect(ship_poly, triangle.vertices_size, debris_polys[i], debris.vertices_size)*/) {
			--i; /* retry */
		}
	}

	subdebris_t osubdebris[MAX_SUBDEBRIS];
	vec2 subdebris_polys[SIZE(osubdebris)][3];
	int osubdebris_n = 0;

	int alive = 1, invincible = 0, score = 0, paused = 0;
	double last_time_s = glfwGetTime();
	int last_paused = 0;
	double fire_accumulator_s = 0.0;
	while(!glfwWindowShouldClose(window)) {
		/* update */
		glfwPollEvents();

		if(!last_paused && glfwGetKey(window, BUTTON_PAUSE)) paused = !paused;

		double now_time_s = glfwGetTime();
		if(paused) last_time_s = now_time_s;
		double dts = now_time_s - last_time_s;
		last_time_s = now_time_s;

		if(dts > EPS && glfwGetKey(window, BUTTON_RESTART)) goto restart;
		last_paused = glfwGetKey(window, BUTTON_PAUSE);
		if(glfwGetKey(window, BUTTON_SLOW)) dts *= 0.25;

		/* movement */
		if(alive) {
#if 1 /* accelerated turning */
			double rotate_acceleration = 30.0*PI, rotate_velocity_max = 10.0*PI, rotate_velocity_min = rad(0.01);
			double rotate_drag_coef = 10.0;

			if(glfwGetKey(window, BUTTON_RIGHT)) ship.rotate_velocity +=-dts*rotate_acceleration;
			if(glfwGetKey(window, BUTTON_LEFT))  ship.rotate_velocity += dts*rotate_acceleration;

			/* drag */
			ship.rotate_velocity += -dts*ship.rotate_velocity*rotate_drag_coef;

			if(abs(ship.rotate_velocity) > rotate_velocity_max) ship.rotate_velocity = (ship.rotate_velocity < 0 ? -rotate_velocity_max : rotate_velocity_max);
			else if(!glfwGetKey(window, BUTTON_RIGHT) && !glfwGetKey(window, BUTTON_LEFT) && abs(ship.rotate_velocity) < rotate_velocity_min) ship.rotate_velocity = 0.0;

			ship.form.rads += dts*ship.rotate_velocity;
#else
			double rotate_speed = 2.0*PI;

			if(glfwGetKey(window, BUTTON_RIGHT)) ship.form.rads +=-dts*rotate_speed;
			if(glfwGetKey(window, BUTTON_LEFT))  ship.form.rads += dts*rotate_speed;
#endif

			double acceleration = 240.0*5.0, velocity_max = 240.0, velocity_min = 10.0, velocity = hypot(ship.velocity_x, ship.velocity_y);
			double velocity_drag_coef = 1.5;

			if(glfwGetKey(window, BUTTON_UP)) {
				ship.velocity_x += dts*acceleration*cos(ship.form.rads + 0.5*PI);
				ship.velocity_y += dts*acceleration*sin(ship.form.rads + 0.5*PI);
			}

			/* drag */
			ship.velocity_x += -dts*velocity_drag_coef*ship.velocity_x;
			ship.velocity_y += -dts*velocity_drag_coef*ship.velocity_y;

			if(velocity > velocity_max) {
				double scale = velocity_max/velocity;
				ship.velocity_x *= scale;
				ship.velocity_y *= scale;
			} else if(!glfwGetKey(window, BUTTON_UP) && velocity < velocity_min) {
				ship.velocity_x = ship.velocity_y = 0.0;
			}

			ship.form.x += dts*ship.velocity_x;
			ship.form.y += dts*ship.velocity_y;

			double fire_time_s = 0.444;
			double bullet_velocity = 400.0;
			fire_accumulator_s += dts;
			int now_fire = glfwGetKey(window, BUTTON_FIRE);
			if(now_fire && fire_accumulator_s > fire_time_s) {
				/* spawn a new bullet */
				if(obullets_n + 1 < MAX_BULLETS) {
					obullets[obullets_n].form.scale_x = 2.5;
					obullets[obullets_n].form.scale_y = 3.5;
					obullets[obullets_n].form.x = ship.form.x + cos(ship.form.rads + 0.5*PI)*ship.form.scale_x;
					obullets[obullets_n].form.y = ship.form.y + sin(ship.form.rads + 0.5*PI)*ship.form.scale_y;
					obullets[obullets_n].form.rads = ship.form.rads;
					obullets[obullets_n].rotate_velocity = 0.0;
					obullets[obullets_n].velocity_x = bullet_velocity*cos(ship.form.rads + 0.5*PI);
					obullets[obullets_n].velocity_y = bullet_velocity*sin(ship.form.rads + 0.5*PI);
					++obullets_n;
				}
				fire_accumulator_s = 0.0;
			}

			/* loop just as the ship's center is offscreen */
			ship.form.x = dmod(ship.form.x, WIDTH);
			ship.form.y = dmod(ship.form.y, HEIGHT);
			ship.form.rads = dmod(ship.form.rads, 2.0*PI);
			transform_poly(triangle.vertices, triangle.vertices_size, &ship.form, ship_poly);
		}

		/* bullets */
		for(int i = 0; i < obullets_n; ++i) {
			obullets[i].form.x += dts*obullets[i].velocity_x;
			obullets[i].form.y += dts*obullets[i].velocity_y;
			/* note: no rotate velocity */
#if 1 /* eliminate bullets outside of bounds */
			if(!(0 <= obullets[i].form.x && obullets[i].form.x <= WIDTH) || !(0 <= obullets[i].form.y && obullets[i].form.y <= HEIGHT)) {
				/* remove bullet */
				--obullets_n;
				memcpy(&obullets[i], &obullets[obullets_n], sizeof(*obullets));
				memcpy(&bullet_polys[i], &bullet_polys[obullets_n], sizeof(*bullet_polys) * triangle.vertices_size);
				--i; /* don't forget to also transform the swapped element */
			}
#else /* repeat bullets outside of bounds */
			obullets[i].form.x = dmod(obullets[i].form.x, WIDTH);
			obullets[i].form.y = dmod(obullets[i].form.y, HEIGHT);
#endif
			transform_poly(triangle.vertices, triangle.vertices_size, &obullets[i].form, bullet_polys[i]);
		}

		/* debris */
		for(int i = 0; i < odebris_n; ++i) {
			odebris[i].form.x += dts*odebris[i].velocity_x;
			odebris[i].form.y += dts*odebris[i].velocity_y;
			odebris[i].form.rads += dts*odebris[i].rotate_velocity;
			odebris[i].form.x = dmod(odebris[i].form.x, WIDTH);
			odebris[i].form.y = dmod(odebris[i].form.y, HEIGHT);
			transform_poly(debris.vertices, debris.vertices_size, &odebris[i].form, debris_polys[i]);

			/* ship collision */
			if(!invincible && alive) alive &= !poly_poly_intersect(ship_poly, triangle.vertices_size, debris_polys[i], debris.vertices_size);

			/* bullet collision */
			for(int j = 0; j < obullets_n; ++j) {
				if(!poly_poly_intersect(bullet_polys[j], triangle.vertices_size, debris_polys[i], debris.vertices_size)) continue;

				/* change the score */
				score += SCORE_DEBRIS;

				/* remove the bullet */
				--obullets_n;
				memcpy(&obullets[j], &obullets[obullets_n], sizeof(*obullets));
				memcpy(&bullet_polys[j], &bullet_polys[obullets_n], sizeof(*bullet_polys)*triangle.vertices_size);

				/* add 4 subdebris in the place of the debris */
				//if(osubdebris_n + 1 < MAX_SUBDEBRIS) { /* note: it's guarenteed to fit */
				osubdebris_n += 4;
				for(int k = osubdebris_n - 4; k < osubdebris_n; ++k) {
					osubdebris[k].i = k - (osubdebris_n - 4);
					osubdebris[k].o.form.rads = odebris[i].form.rads;
					osubdebris[k].o.form.x = odebris[i].form.x;// + cos(odebris[i].form.rads)*odebris[i].form.scale_x;
					osubdebris[k].o.form.y = odebris[i].form.y;// + sin(odebris[i].form.rads)*odebris[i].form.scale_y;
					osubdebris[k].o.form.scale_x = odebris[i].form.scale_x * ((rand()%200)/200.0 + 0.5);
					osubdebris[k].o.form.scale_y = odebris[i].form.scale_y * ((rand()%200)/200.0 + 0.5);
					osubdebris[k].o.rotate_velocity = ((rand()%2000)/2000.0*0.5*PI - 0.25*PI)*4.0;
					osubdebris[k].o.velocity_x = ((rand()%500)/5.0 - 50.0)*2.0;
					osubdebris[k].o.velocity_y = ((rand()%500)/5.0 - 50.0)*2.0;
				}
				//}

				/* remove the debris */
				--odebris_n;
				memcpy(&odebris[i], &odebris[odebris_n], sizeof(*odebris));
				memcpy(&debris_polys[i], &debris_polys[odebris_n], sizeof(*debris_polys)*debris.vertices_size);
				--i; /* don't forget to also transform the swapped element */

				/* dont worry about other bullet collisions, since this debris is dead */
				break;
			}
		}

		for(int i = 0; i < osubdebris_n; ++i) {
			osubdebris[i].o.form.x += dts*osubdebris[i].o.velocity_x;
			osubdebris[i].o.form.y += dts*osubdebris[i].o.velocity_y;
			osubdebris[i].o.form.rads += dts*osubdebris[i].o.rotate_velocity;
			osubdebris[i].o.form.x = dmod(osubdebris[i].o.form.x, WIDTH);
			osubdebris[i].o.form.y = dmod(osubdebris[i].o.form.y, HEIGHT);
			transform_poly(subdebris[osubdebris[i].i].vertices, 3, &osubdebris[i].o.form, subdebris_polys[i]);

			/* ship collision */
			if(!invincible && alive) alive &= !poly_poly_intersect(ship_poly, triangle.vertices_size, subdebris_polys[i], 3);

			/* bullet collision */
			for(int j = 0; j < obullets_n; ++j) {
				if(!poly_poly_intersect(bullet_polys[j], triangle.vertices_size, subdebris_polys[i], 3)) continue;

				score += SCORE_SUBDEBRIS;

				/* remove the bullet */
				--obullets_n;
				memcpy(&obullets[j], &obullets[obullets_n], sizeof(*obullets));
				memcpy(&bullet_polys[j], &bullet_polys[obullets_n], sizeof(*bullet_polys)*triangle.vertices_size);

				/* remove the subdebris */
				--osubdebris_n;
				memcpy(&osubdebris[i], &osubdebris[osubdebris_n], sizeof(*osubdebris));
				memcpy(&subdebris_polys[i], &subdebris_polys[osubdebris_n], sizeof(*subdebris_polys)*3);
				--i; /* don't forget to also transform the swapped element */

				/* dont worry about other bullet collisions, since this subdebris is dead */
				break;
			}
		}

		/* render */
		gl(Clear(GL_COLOR_BUFFER_BIT));

		mat4x4 cam_view;
		mat4x4_look_at(
			cam_view,
			(vec3) { 0.0, 0.0,-1.0 },
			(vec3) { 0.0, 0.0, 0.0 },
			(vec3) { 0.0, 1.0, 0.0 }
		);

		mat4x4 cam_proj;
		mat4x4_ortho(
			cam_proj,
			0.0,-WIDTH,
			0.0, HEIGHT,
			-1.0, 1.0
		);

		mat4x4 vp; /* = cam_proj * cam_view */
		mat4x4_mul(vp, cam_proj, cam_view);

		bind_program(prog_poly);
		if(alive) {
			push_mvp(vp, &ship.form, prog_poly_mvp);
			render_mesh2d_vertices(&triangle, GL_LINE_LOOP);
		}
		for(int i = 0; i < obullets_n; ++i) {
			push_mvp(vp, &obullets[i].form, prog_poly_mvp);
			render_mesh2d_vertices(&triangle, GL_LINE_LOOP);
		}
		for(int i = 0; i < odebris_n; ++i) {
			push_mvp(vp, &odebris[i].form, prog_poly_mvp);
			render_mesh2d_vertices(&debris, GL_LINE_LOOP);
		}
		for(int i = 0; i < osubdebris_n; ++i) {
			push_mvp(vp, &osubdebris[i].o.form, prog_poly_mvp);
			render_mesh2d_vertices(&subdebris[osubdebris[i].i], GL_LINE_LOOP);
		}
		unbind_program();

		char text[256] = { 0 };
		transform_t form = IFORM;

#if 1 /* fps */
		sprintf(text, "fps:%.0f", 1.0/dts);
		form.scale_x = C64_X;
		form.scale_y = C64_Y;
		form.x = 0;
		form.y = HEIGHT + form.scale_y*0.5;
		render_text(text, vp, &form);
#endif

#if 1 /* score */
		max_score = max(max_score, score);

		sprintf(text, "max:%d score:%d", max_score, score);
		form.scale_x = 2.0*C64_X;
		form.scale_y = 2.0*C64_Y;
		form.x = 0;
		form.y = form.scale_y*3.0;
		render_text(text, vp, &form);
#endif

		if(alive) {
#if 0 /* debug text */
			sprintf(text, "p  %.1f %.1f\nv  %.1f %.1f\nr  %.1f\nrv %.1f",
				ship.form.x, ship.form.y,
				ship.velocity_x, ship.velocity_y,
				deg(ship.form.rads),
				deg(ship.rotate_velocity)
			);

			form.scale_x = 0.5*C64_X;
			form.scale_y = 0.5*C64_Y;
			form.x = ship.form.x - form.scale_x*13;
			form.y = ship.form.y - ship.form.scale_y - ship.form.scale_y*0.2;
			render_text(text, vp, &form);
#endif

			if(!odebris_n) {
#if 1 /* winner */
				const char* on_str = "winner!";
				form.scale_x = 5.0*C64_X;
				form.scale_y = 5.0*C64_Y;
				form.x = WIDTH/2.0 - strlen(on_str)*form.scale_x;
				form.y = HEIGHT/2.0 + 2.0*form.scale_y;
				render_text(on_str, vp, &form);
#endif
			}
		} else {
#if 1 /* loser */
			const char* on_str = "game over!";
			form.scale_x = 5.0*C64_X;
			form.scale_y = 5.0*C64_Y;
			form.x = WIDTH/2.0 - strlen(on_str)*form.scale_x;
			form.y = HEIGHT/2.0 + 2.0*form.scale_y;
			render_text(on_str, vp, &form);
#endif
		}

		/* display */
		glfwSwapBuffers(window);
	}

	setdown_graphics();

	return 0;
}
