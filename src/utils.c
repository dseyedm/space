#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#if 0
int intersect_line(pt_t* l0_a, pt_t* l0_b, pt_t* l1_a, pt_t* l1_b, pt_t* out_p) {
	pt_ t;
	double r, s;
	double denom, num1, num2;

	assert((a.x != b.x || a.y != b.y) && (c.x != d.x || c.y != d.y));

	num1 = (a.y - c.y)*(d.x - c.x) - (a.x - c.x)*(d.y - c.y);
	num2 = (a.y - c.y)*(b.x - a.x) - (a.x - c.x)*(b.y - a.y);
	denom = (b.x - a.x)*(d.y - c.y) - (b.y - a.y)*(d.x - c.x);

	if(fabs(denom) >= EPS) {
		r = num1 / denom;
		s = num2 / denom;
		if(0-EPS <= r && r <= 1+EPS &&
		   0-EPS <= s && s <= 1+EPS) {
			/* always do this part if we are interested in lines instead */
			/* of line segments                                          */
			p.x = a.x + r*(b.x - a.x);
			p.y = a.y + r*(b.y - a.y);
			return 1;
		}
		return 0;
	}
	if (fabs(num1) >= EPSILON) return 0;
	/* I am not using "fuzzy comparisons" here, because the comparisons */
	/* are based on the input, not some derived quantities.  You may    */
	/* want to change that if the input points are computed somehow.    */

	/* two lines are the "same".  See if they overlap */
	if (a.x > b.x || (a.x == b.x && a.y > b.y)) {
		t = a;
		a = b;
		b = t;
	}
	if (c.x > d.x || (c.x == d.x && c.y > d.y)) {
		t = c;
		c = d;
		d = t;
	}
	if (a.x == b.x) {
		/* vertical lines */
		if (b.y == c.y) {
			p = b;
			return 1;
		} else if (a.y == d.y) {
			p = a;
			return 1;
		} else if (b.y < c.y || d.y < a.y) {
			return 0;
		}
		return -1;
	}
	if (b.x == c.x) {
		p = b;
		return 1;
	} else if (a.x == d.x) {
		p = a;
		return 1;
	} else if (b.x < c.x || d.x < a.x) {
		return 0;
	}
	return -1;
}
#endif

char* load_file(const char* file) {
	FILE* fp = fopen(file, "rb");
	if(fp == NULL) {
		fprintf(stderr, "failed to load \"%s\"", file);
		return NULL;
	}

	fseek(fp, 0, SEEK_END);
	size_t size = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	char* contents = malloc(sizeof(*contents) * size + 1);
	if(contents == NULL) return NULL;

	fread(contents, size, 1, fp);
	contents[size] = (char)0;

	return contents;
}
