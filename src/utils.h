#ifndef UTILS_H
#define UTILS_H

#define SIZE(a) (sizeof(a) / sizeof(a[0]))
#define UNUSED(a) (void)(a)

#ifdef OS_LIN
#define min(a, b) (b < a ? b : a)
#define max(a, b) (b > a ? b : a)
#endif

#define PI 3.141592653589793
#define EPS 1e-2

/* 16:9 * 64 */
#define WIDTH 1024
#define HEIGHT 576
#define ASPECT (double)WIDTH/HEIGHT

#include "linmath/linmath.h"
#include <math.h>

typedef struct {
	double x, y;
} pt_t;

static inline double rad(double a) {
	return a * PI / 180.0;
}

static inline double deg(double a) {
	return a * 180.0 / PI;
}

static inline double lerp(double a, double t, double b) {
	return a + (b - a) * t;
}

static inline double ndc_y(double wc) {
	return wc / (0.5 * HEIGHT) - 1.0;
}

static inline double ndc_x(double wc) {
	return wc / (0.5 * WIDTH) - 1.0;
}

static inline double dmod(double t, double m) {
	return t - m * floor(t / m);
}

static inline double dist2(vec2 a, vec2 b) {
	double t0 = a[0] - b[0];
	double t1 = a[1] - b[1];
	return t0*t0 + t1*t1;
}

static inline double dist(vec2 a, vec2 b) {
	double t0 = a[0] - b[0];
	double t1 = a[1] - b[1];
	return sqrt(t0*t0 + t1*t1);
}

static inline void mat4x4_scale_X(mat4x4 m, double s) {
	vec4_scale(m[0], m[0], s);
}

static inline void mat4x4_scale_Y(mat4x4 m, double s) {
	vec4_scale(m[1], m[1], s);
}

static inline void mat4x4_scale_Z(mat4x4 m, double s) {
	vec4_scale(m[2], m[2], s);
}

static inline double vec2_dot(vec2 v0, vec2 v1) {
	return v0[0]*v1[0] + v0[1]*v1[1];
}

/* consider building a rotation matrix */
static inline void vec2_rotate(vec2 v, double rads) {
	double x = v[0], y = v[1];
	v[0] = x*cos(rads) - y*sin(rads);
	v[1] = x*sin(rads) + y*cos(rads);
}

/* taken from howard cheng's code library */
/* returns 1 if intersect at a point, 0 if not, -1 if the lines coincide */
int intersect_line(pt_t* a, pt_t* b, pt_t* c, pt_t* d, pt_t* p);

/* one must free the returned contents */
char* load_file(const char* file);

#endif
