#include "gfx/vertex_array.h"

const vao_t VAO = IVAO;

extern inline void create_vao(vao_t* vao);
extern inline void destroy_vao(vao_t* vao);
extern inline void bind_vao(vao_t vao);
extern inline void unbind_vao();
extern inline vao_t bound_vao();
